#include "Bullet.h"
#include "Entity.h"

const int MAX_BULLETS = 20;

#pragma once
class Player : public Entity
{
public:
	Bullet bullets[MAX_BULLETS];

	void SetMovementKeys(unsigned int moveLeftKey, unsigned int moveRightKey, unsigned int rotateLeftKey, unsigned int rotateRightKey, unsigned int moveUpKey, unsigned int moveDownKey);
    void SetMovementExtremes(unsigned int leftMovementExtreme, unsigned int rightMovementExtreme, unsigned int upMovementExtreme, unsigned int downMovementExtreme );

    virtual void Update(float delta);
    virtual void Draw();

    void Shoot(unsigned int textureID, float delta);
    Bullet& GetInactiveBullet();

    void SetSpeed(float speed);
    float GetSpeed();

	void SetAngle(float angle);
	float GetAngle();

    Player();
    ~Player();

    unsigned int iShootKey;

private:
	unsigned int moveUpKey;
    unsigned int moveDownKey;
    unsigned int moveLeftKey;
    unsigned int moveRightKey;
	unsigned int rotateLeftKey;
	unsigned int rotateRightKey;
    
    float speed;
	float angle;
	
    unsigned int leftMovementExtreme;
    unsigned int rightMovementExtreme;
    unsigned int upMovementExtreme;
    unsigned int downMovementExtreme;

    float currentReloadBulletTime;
    float maxBulletReloadTime;
};