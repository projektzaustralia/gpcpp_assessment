#pragma once
class Entity
{
public:
    Entity();

    void SetSize(float width, float height);
    void SetPosition(float x, float y);

    virtual void Update(float delta) = 0;
    virtual void Draw() = 0;

    void SetSpriteID(unsigned int spriteID);
    unsigned int GetSpriteID();

    void SetWidth(float width);
    void SetHeight(float height);

    float GetWidth();
    float GetHeight();

    void SetX(float x);
    void SetY(float y);

    float GetX();
    float GetY();

    float GetVelocityX();
    float GetVelocityY();

    bool GetIsActive();

    int GetHealthPoints();

    void SetVelocityX( float fVelocityX );
    void SetVelocityY( float fVelocityY );

    void SetIsActive( bool bIsActive );

    void SetHealthPoints( int iHealthPoints );

	bool isActive;

    ~Entity();

protected:

    unsigned int spriteID;

    float width;
    float height;
	float health;
	float fVelocityX;
	float fVelocityY;
    float x;
    float y;

};