#pragma once

#include "BaseState.h"
#include "Player.h"
#include "Enemy.h"
#include "EnemySpawnPoint.h"
#include "Entity.h"
#include <vector>

const int NUM_ALIENS = 10;
const int NUM_SPAWNPOINTS = 1;
const int MAX_SPAWNPOINTS = 4;
const int MAX_ALIENS = 30;

class GameState : public BaseState
{
public:
	GameState(void);
	~GameState(void);
	
	void Initialize();
	void Update(float a_fTimeStep, StateMachine* a_pSM);
	void Draw();
	void Destroy();

private:
	void CreateEnemySpawnPoints();
	void MoveEnemies(float speed, int direction, float delta);
	void DrawEnemies();

    void PlayerLogic(Player* player, float delta);
    void EnemySpawnPointLogic(Enemy* enemy, EnemySpawnPoint* enemySpawnPoint, float delta);
	void EnemyLogic(Enemy* enemy, float delta);

	void CreateEnemies();

	bool CheckCollision(float x1, float y1, float x2, float y2, float distance);

private:	
    std::vector<Entity*> gameObjects;

	unsigned int iGame;
	unsigned int bulletTexture;
	unsigned int enemyTexture;
	int score;
	int highscore;
	int direction;
	char szP1Score[20];
	char szHighscore[20];

	bool gameOver;
	float gameOverTimer;
};