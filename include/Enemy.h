#pragma once

#include "AIE.h"
#include "Entity.h"

class Enemy : public Entity
{
public:
    Enemy();
	bool isActive;

    void Update(float delta);
    void Draw();

    void SetSpeed(float speed);
    float GetSpeed();

	void SetAngle( float angle );
	float GetAngle();

	void InitialiseEnemy(float x, float y, float velocityX, float velocityY, unsigned int spriteID);
	float velocityX;
	float velocityY;

	float currentSpawnTime;
	float maxSpawnTime;
    ~Enemy();

private:


    float speed;
	float angle;
};