#include "SpawnPoint.h"
#include "Enemy.h"
#include "AIE.h"
#include <vector>;

const int MAX_ENEMIES = 10;
const int MAX_SHIELD = 100;
const float fEnemySpeed = 100.f;

#pragma once
class EnemySpawnPoint : public SpawnPoint
{
public:
	EnemySpawnPoint();
	~EnemySpawnPoint();
	
	std::vector<Enemy> enemies;
	void ReleaseEnemy(unsigned int textureID, float delta);
	Enemy& GetInactiveEnemy();

	void Update(float delta);
	void Draw();
	float maxSpawnEnemyTime;
	float currentSpawnEnemyTime;
	void SetAngle(float angle);
	float GetAngle();

private:
	float angle;
};

