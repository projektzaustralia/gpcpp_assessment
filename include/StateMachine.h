#pragma once

#include "BaseState.h"
#include <list>

class StateMachine
{
public:
	StateMachine(void);
	~StateMachine(void);

	void PushState(BaseState* gameState);
	BaseState* PopState();
	BaseState* SwitchState(BaseState* gameState);

	void Update(float a_fTimeStep);
	void Draw();

private:
	std::list<BaseState*> stateStack;
};