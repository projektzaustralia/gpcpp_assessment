#include "EnemySpawnPoint.h"
#include "GameState.h"
#include "Enemy.h"
#define PI 3.14159265

extern const int iScreenWidth;
extern const int iScreenHeight;

EnemySpawnPoint::EnemySpawnPoint()
{
	currentSpawnEnemyTime = 0.0f;
	maxSpawnEnemyTime = 1.f;
	SetSpriteID( CreateSprite("./PNG/ufoRed.png",  50, 50, true) );
	SetIsActive( true );
	SetX( iScreenWidth*0.065f );
	SetY( iScreenHeight-75.f );
	SetVelocityX( 0.f );
	SetVelocityY( 0.f );
	SetHealthPoints( 100 );
	SetAngle((atan2(GetY(), GetX())*180/PI));

	for(int i = 0; i < NUM_ALIENS; i++)
		enemies.push_back(Enemy());
}

void EnemySpawnPoint::Update(float delta)
{
}

void EnemySpawnPoint::Draw()
{
    if(isActive)
    {
		MoveSprite(GetSpriteID(), GetX(), GetY());
        DrawSprite(GetSpriteID());
    }
}
void EnemySpawnPoint::ReleaseEnemy(unsigned int textureID, float delta)
{

	if( isActive && currentSpawnEnemyTime >= maxSpawnEnemyTime)
    {
		GetInactiveEnemy().InitialiseEnemy(x, y, cos(angle*(PI/180)), sin(angle*(PI/180)), textureID);
        currentSpawnEnemyTime = 0.0f;
    }

    currentSpawnEnemyTime += delta;
}

Enemy& EnemySpawnPoint::GetInactiveEnemy()
{
    for(int i = 0; i < NUM_ALIENS; i++)
    {
        if(!enemies[i].isActive)
        {
            return enemies[i];
        }
    }

    return enemies[0];
}
void EnemySpawnPoint::SetAngle(float angle)
{
	this->angle=angle;
}
float EnemySpawnPoint::GetAngle()
{
	return angle;
}
EnemySpawnPoint::~EnemySpawnPoint(void)
{
}
