#include "AIE.h"
#include <string>
#include <cmath>
#include <time.h>
#include <assert.h>
#include <crtdbg.h>
#include <iostream>
#include "StateMachine.h"
#include "MainMenuState.h"

extern const int iScreenWidth = 980;
extern const int iScreenHeight = 661;


int main( int argc, char* argv[] )
{	
	Initialise( iScreenWidth, iScreenHeight, false, "Space Invaders" );
	SetBackgroundColour( SColour( 0x00, 0x00, 0x00, 0xFF ) );

	StateMachine state;
	state.PushState( new MainMenuState() );

	do 
	{
		ClearScreen();
		float fDeltaT = GetDeltaTime();

		state.Update(fDeltaT);
		state.Draw();

	} while ( FrameworkUpdate() == false );
	
	Shutdown();

	return 0;
}