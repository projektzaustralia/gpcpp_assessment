#include "GameState.h"
#include "AIE.h"
#include "StateMachine.h"
#include "Player.h"
#include "Bullet.h"
#include "EnemySpawnPoint.h"
#include "Highscores.h"
#include "LeaderboardState.h"

#define PI 3.14159265

extern const int iScreenWidth;
extern const int iScreenHeight;
bool allDead = false;
float ex = 0;
float ey = 0;
float px = 0;
float py = 0;

const char* pkScore1 = "<Score 1>";
const char* pkScore2 = "<Score 2>";
const char* pkHiScore = "<HiScore>";

GameState::GameState(void)
{
	score = 0;
	direction = 1;
	bulletTexture = -1;
}

GameState::~GameState(void)
{
	for(auto object : gameObjects)		
		delete object;
}

void GameState::Initialize()
{	
	bulletTexture = CreateSprite("./PNG/ufoYellow.png", 10, 10, true);
	enemyTexture = 	CreateSprite("./PNG/Enemies/enemyBlack1.png", 72.f, 61.f, true);
	iGame = CreateSprite("./Backgrounds/darkPurple.png", iScreenWidth, iScreenHeight, true);

	Player* player = new Player();
	gameObjects.push_back(player);
	
	MoveSprite( player->GetSpriteID(), player->GetX(), player->GetY() );
	MoveSprite(iGame, iScreenWidth/2, iScreenHeight/2);

	CreateEnemySpawnPoints();

	Highscores scores;
	scores.LoadScores();
	if(scores.IsEmpty())
	{
		highscore = 0;
	}
	else
	{
		scores.SortScores();
		highscore = *scores.GetScores().begin();
	}
}

void GameState::Destroy()
{
	for(auto object : gameObjects)
    {		
		if(object != NULL && object->isActive )
			DestroySprite(object->GetSpriteID());
    }
    DestroySprite(bulletTexture);
}

void GameState::PlayerLogic(Player* player, float delta)
{
	if(player == NULL)
		return;

 	player->Shoot(bulletTexture, delta);

    for(int i = 0; i < MAX_BULLETS; i++)
    {
        player->bullets[i].Update(delta);
        player->bullets[i].Draw();
    }

    for(auto alienShip : gameObjects)
    {
        if(dynamic_cast<Enemy*>(alienShip) != 0)
        {
            Enemy* enemyShip = dynamic_cast<Enemy*>(alienShip);
            for(int i = 0; i < MAX_BULLETS; i++)
            {
                if(CheckCollision(player->bullets[i].x, player->bullets[i].y, enemyShip->GetX(), enemyShip->GetY(), 30.0f) && enemyShip->GetIsActive() && player->bullets[i].isActive)
                {
                    enemyShip->SetIsActive(false);
                    player->bullets[i].isActive = false;
                    score++;
                }
            }
        }
    }
}

void GameState::EnemySpawnPointLogic(Enemy* enemy, EnemySpawnPoint* enemySpawnPoint, float delta)
{
	if(enemySpawnPoint != NULL && enemy != NULL)
	{
		for(int i = 0; i < NUM_ALIENS; i++)
		{
			enemySpawnPoint->ReleaseEnemy( enemyTexture, delta);
			enemySpawnPoint->enemies[i].Update(delta);
			enemySpawnPoint->enemies[i].Draw();
		}
	}
}
void GameState::EnemyLogic(Enemy* enemy, float delta)
{
	  //  if(enemy->GetIsActive() == true)
	  //  {
			//allDead = false;
	  //  }
}

void GameState::Update(float a_fTimeStep, StateMachine* a_pSM)
{
	if( IsKeyDown( KEY_ESCAPE ) )
    {
		BaseState* lastState = a_pSM->PopState();
		delete lastState;
		return;
    }
	if(gameOver)
	{
		//gameOverTimer -= a_fTimeStep;
		if(/*gameOverTimer <= 0*/ IsKeyDown( 'H' ) )
		{
			LeaderboardState* leaderboard = new LeaderboardState();
			leaderboard->SetPlayersScore(score);
			BaseState* lastState = a_pSM->SwitchState(leaderboard);
			delete lastState;
		}
		return;
	}
	
	//allDead = true;

	for(auto object : gameObjects)
    {
        if(dynamic_cast<Player*>(object) != 0)
        {
            PlayerLogic(dynamic_cast<Player*>(object), a_fTimeStep);
		}
		if(dynamic_cast<EnemySpawnPoint*>(object) != 0)
        {
			EnemySpawnPointLogic(dynamic_cast<Enemy*>(object), dynamic_cast<EnemySpawnPoint*>(object), a_fTimeStep);
		}
		if(dynamic_cast<Enemy*>(object) != 0)
        {
            EnemyLogic(dynamic_cast<Enemy*>(object), a_fTimeStep);
		}
	object->Update(a_fTimeStep);        
    object->Draw();
	}
	//if(allDead == true) 
	//{
	//	gameOver = true;
	//	return;
	//}
}

void GameState::Draw()
{
	for(auto object : gameObjects)
    {
        object->Draw();
    }

    sprintf(szP1Score, "%d", score);	   
    DrawString( pkScore1,	iScreenWidth * 0.025f,	75 );
    DrawString( szP1Score,	iScreenWidth * 0.025f,	45 );
   
    sprintf(szHighscore, "%d", highscore);
    DrawString( pkHiScore,	iScreenWidth * 0.425f, 75 );
    DrawString( szHighscore,	iScreenWidth * 0.425f, 45 );

}

bool GameState::CheckCollision(float x1, float y1, float x2, float y2, float distance)
{
    float d = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));

    if(d < distance)
        return true;
    else
        return false;
}


void GameState::CreateEnemySpawnPoints()
{
	int i = 0;
	while(i++ != NUM_SPAWNPOINTS)
    {
        EnemySpawnPoint* enemySpawnPoint = new EnemySpawnPoint();
        gameObjects.push_back(enemySpawnPoint);
    }
}