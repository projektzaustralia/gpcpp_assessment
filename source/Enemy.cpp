//
//  Enemy.cpp
//  AIE Basic Framework
//
//  Created by David Zammit on 7/08/2014.
//  Copyright (c) 2014 AIE. All rights reserved.
//

#include "Enemy.h"
#include <math.h>
#define PI 3.14159265

extern const int iScreenWidth;
extern const int iScreenHeight;

Enemy::Enemy()
{
    SetIsActive( true );
	SetHealthPoints( 10.f );
	SetVelocityX( 0 );
    SetVelocityY( -10.f );
    SetWidth( 52.f );
    SetHeight( 41.f );
	SetSpeed( 10.f );
	SetX( 200 );
	SetY( iScreenHeight-50 );

	currentSpawnTime = 0.0f;
	maxSpawnTime = 0.25f;
}
void Enemy::Draw()
{
    if( GetIsActive() )
    {
        MoveSprite( GetSpriteID(), GetX(), GetY() );
        DrawSprite( GetSpriteID()  );
    }
};

void Enemy::Update(float delta)
{
    if(isActive)
    {
        x -= speed*velocityX;
        y -= speed*velocityY;
    }

	if(y < 0)
	{
		isActive = false;
	}
}

void Enemy::InitialiseEnemy(float x, float y, float velocityX, float velocityY, unsigned int spriteID)
{
    this->x = x;
    this->y = y;
    this->velocityX = velocityX;
    this->velocityY = velocityY;
	this->spriteID = spriteID;
    isActive = true;
}

void Enemy::SetAngle( float angle )
{
	this->angle = angle;
}
float Enemy::GetAngle()
{
	return angle;
}
void Enemy::SetSpeed( float speed )
{
	this->speed = speed;
}
float Enemy::GetSpeed()
{
	return speed;
}
Enemy::~Enemy()
{
    
}