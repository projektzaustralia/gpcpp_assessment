#include "Entity.h"


Entity::Entity()
{
	spriteID = 0;
    width = 0;
    height = 0;
	health = 0;
	fVelocityX = 0;
	fVelocityY = 0;
    x = 0;
    y = 0;
}
void Entity::SetSize(float width, float height)
{
    this->width = width;
    this->height = height;
}

void Entity::SetPosition(float x, float y)
{
    this->x = x;
    this->y = y;
}

void Entity::SetSpriteID(unsigned int spriteID)
{
    this->spriteID = spriteID;
}

unsigned int Entity::GetSpriteID()
{
    return spriteID;
}

void Entity::SetWidth(float width)
{
    this->width = width;
}

void Entity::SetHeight(float height)
{
    this->height = height;
}

float Entity::GetWidth()
{
    return width;
}

float Entity::GetHeight()
{
    return height;
}

void Entity::SetX(float x)
{
    this->x = x;
}

void Entity::SetY(float y)
{
    this->y = y;
}

float Entity::GetX()
{
    return x;
}

float Entity::GetY()
{
    return y;
}
bool Entity::GetIsActive()
{
    return isActive;
};
int Entity::GetHealthPoints()
{
	return health;
};
float Entity::GetVelocityX()
{
    return fVelocityX;
};
float Entity::GetVelocityY()
{
    return fVelocityY;
};
void Entity::SetVelocityX( float fVelocityX )
{
    this->fVelocityX = fVelocityX;
};
void Entity::SetVelocityY( float fVelocityY )
{
    this->fVelocityY = fVelocityY;
};
void Entity::SetIsActive( bool isActive )
{
	this->isActive = isActive;
};
void Entity::SetHealthPoints( int health )
{
	this->health = health;
};
Entity::~Entity()
{
}