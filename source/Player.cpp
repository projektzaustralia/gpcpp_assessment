#include "Player.h"
#include "AIE.h"
#include <math.h>
#define PI 3.14159265

extern const int iScreenWidth;
extern const int iScreenHeight;

Player::Player()
{
	currentReloadBulletTime = 0.0f;
	maxBulletReloadTime = 0.15f;

	SetSize( 77.f, 54.f );
    SetMovementKeys('A', 'D', 'O', 'P', 'W', 'S');
    iShootKey = KEY_SPACE;
	SetMovementExtremes( 0, iScreenWidth, iScreenHeight, 0);
    SetSpriteID(CreateSprite( "./PNG/playerShip3_red.png", GetWidth(), GetHeight(), true));
    SetX(iScreenWidth * 0.5f);
    SetY(125.f);
    SetSpeed(150.f);
	SetAngle((atan2(GetY(), GetX())*180/PI)+78);
}
void Player::SetMovementKeys( unsigned int moveLeftKey, unsigned int moveRightKey, unsigned int rotateLeftKey, unsigned int rotateRightKey, unsigned int moveUpKey,unsigned int moveDownKey )
{
    this->moveLeftKey = moveLeftKey;
    this->moveRightKey = moveRightKey;
	this->rotateLeftKey = rotateLeftKey;
	this->rotateRightKey = rotateRightKey;
	this->moveUpKey = moveUpKey;
	this->moveDownKey = moveDownKey;
}

void Player::SetMovementExtremes( unsigned int leftMovementExtreme, unsigned int rightMovementExtreme, unsigned int upMovementExtreme, unsigned int downMovementExtreme )
{
    this->leftMovementExtreme = leftMovementExtreme;
    this->rightMovementExtreme = rightMovementExtreme;
	this->upMovementExtreme = upMovementExtreme;
    this->downMovementExtreme = downMovementExtreme;

}

void Player::Update(float delta)
{
    if(IsKeyDown(moveLeftKey))
    {
        x -= delta * speed;
        if(x < (leftMovementExtreme + width*.5f))
        {
            x = (leftMovementExtreme + width*.5f);
        }
    }

    if( IsKeyDown(moveRightKey))
    {
        x += delta * speed;
        if( x > (rightMovementExtreme - width*.5f))
        {
            x = (rightMovementExtreme - width*.5f);
        }
    }

	if(IsKeyDown(moveUpKey))
    {
        y += delta * speed;
        if(y > (upMovementExtreme - height*.5f))
        {
            y = (upMovementExtreme - height*.5f);
        }
    }

    if( IsKeyDown(moveDownKey))
    {
        y -= delta * speed;
		if( y < (downMovementExtreme + height*.5f))
        {
			y = (downMovementExtreme + height*.5f);
        }
    }
	if( IsKeyDown(rotateLeftKey) )
	{
		RotateSprite(spriteID, 5.f);
		SetAngle(GetAngle()+ 5.f);
	}

	if( IsKeyDown(rotateRightKey) )
	{
		RotateSprite(spriteID, -5.f);
		SetAngle(GetAngle()+ -5.f);
	}
}
void Player::Shoot(unsigned int textureID, float delta)
{

    if(IsKeyDown(iShootKey) && currentReloadBulletTime >= maxBulletReloadTime)
    {
		GetInactiveBullet().InitialiseBullet(x, y, cos(GetAngle()*(PI/180)), sin(GetAngle()*(PI/180)), textureID);
        currentReloadBulletTime = 0.0f;
    }

    currentReloadBulletTime += delta;
}

Bullet& Player::GetInactiveBullet()
{
    for(int i = 0; i < MAX_BULLETS; i++)
    {
        if(!bullets[i].isActive)
        {
            return bullets[i];
        }
    }

    return bullets[0];
}
void Player::Draw()
{
    MoveSprite(spriteID, x, y);
    DrawSprite(spriteID);
}

void Player::SetSpeed(float speed)
{
    this->speed = speed;
}

float Player::GetSpeed()
{
    return speed;
}
void Player::SetAngle(float angle)
{
	this->angle=angle;
}
float Player::GetAngle()
{
	return angle;
}
Player::~Player()
{
}
